# React App to show JSON List

React is a Javascript library, by Facebook, that is used for building user interfaces. We will be using the following:

* Webpack - A module bundler
* Babel - A Javascript compiler
* ES6 - A relatively new Javasript standard
* Yarn - A package manager
* React - As expected

To run this app, run below code in CLI

```
yarn add
yarn start

```

Project architecture

```
|-- public
     |-- fonts
          |-- opensans-regular.woff
     |-- img
          |-- logo.svg
     |-- js
          |-- users.json
     |-- styles
          |-- main.less
     |-- favicon.ico
|-- views
     |-- components
          |-- Header.js
          |-- Home.js
          |-- Loader.js
          |-- Usercard.js
          |-- Userdetail.js
     |-- master
          |-- App.js
          |-- Main.js
     |-- index.html
     |-- index.js
|-- .babelrc
|-- .gitignore
|-- package.json
|-- postcss.config.js
|-- readme.md
|-- webpack.config.js
|-- yarn.lock

```
