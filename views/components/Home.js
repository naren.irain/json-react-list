import React, {Component} from 'react';
import axios from 'axios';
import Usercard from './Usercard.js';
import Loader from './Loader.js';

export default class Home extends Component {

    constructor() {
        super();

        this.state = {
            userList: []
        };
    }

    componentWillMount() {
        axios.get('../../public/js/users.json')
        .then( response => {
            this.setState({
                userList: response.data
            });
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    render() {

        const usersList = this.state.userList;
        let usersListBlock = <Loader />;

        if(usersList.length > 0) {
            usersListBlock = usersList.map( obj => {
                return (
                    <Usercard key={obj.id} id={obj.id} imgPath={obj.avatar_url} name={obj.name} />
                )
            })
        }

        return(
            <div className="page homePage">
                <div className="row container">
                    {usersListBlock}
                </div>
            </div>
        )
    }
}