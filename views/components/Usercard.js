import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Usercard extends Component {

	render() {

		return(
			<div className="col s12 m6">
				<div className="card horizontal">
					<div className="card-image">
						<img src={this.props.imgPath} />
					</div>
					<div className="card-stacked">
						<div className="card-content">
							<h4 className="header">{this.props.name}</h4>
						</div>
						<div className="card-action">
							<Link to={`/user:${this.props.id}`}>View Profile</Link>
						</div>
					</div>
				</div>
			</div>
		);
	}
};
