import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Loader from './Loader.js';

export default class Userdetail extends Component {

    constructor() {
        super();

        this.state = {
            userDetail: null
        };
    }

    componentWillMount() {
        let userId = (this.props.match.params.userId).split(':')[1];
        console.log(userId);

        axios.get('../../public/js/users.json')
        .then( response => {
            let usersList = response.data;
            let userData = usersList.filter( c => c.id == userId );

            this.setState({
                userDetail: userData[0]
            });
        })
        .catch(function (error) {
            console.log(error);
        });

    }

    render() {

        console.log(this.state.userDetail);

        let Userdetail = <Loader />;
        if(this.state.userDetail != null) {
            Userdetail = (
                <div className="row container left-align">
                    <p></p>
                    <p>
                        <Link to="/" className="waves-effect waves-light btn"><i className="material-icons" dangerouslySetInnerHTML={{__html: "arrow_back"}}></i> Back to Users List</Link>
                    </p>
                    <div className="row">
                        <div className="col s12">
                            <div className="card detailCard">
                                <div className="card-image">
                                    <img src={this.state.userDetail.cover_url} alt={this.state.userDetail.name} />
                                    <div className="btn-floating halfway-fab media"><img src={this.state.userDetail.avatar_url} className="responsive-img" alt={this.state.userDetail.name} /></div>
                                </div>
                                <div className="card-content">
                                    <h3 className="card-title">{this.state.userDetail.name}</h3>
                                    <h5>{this.state.userDetail.role}</h5>
                                    <hr/>
                                    <p>{this.state.userDetail.about}</p>
                                    <p><b>Location:</b> {this.state.userDetail.city}</p>
                                    <p><b>Email:</b> {this.state.userDetail.email}</p>
                                    <p><b>Phone:</b> {this.state.userDetail.phone}</p>
                                    <p><b>Link:</b> <a href={this.state.userDetail.url} target="_blank">{this.state.userDetail.url}</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }

        return (
            <div className="page userDetailPage">
                {Userdetail}
            </div>
        )
    }
}