import React, {Component} from 'react';
import Header from '../components/Header.js';
import Main from './Main.js';

export default class App extends Component {
  render() {
    return (
     <div>
        <Header title="List Users - JSON and React" />
        <Main />
     </div>
    )
  }
}