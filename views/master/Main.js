import React, {Component} from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from '../components/Home.js';
import Userdetail from '../components/Userdetail.js';

export default class Main extends Component {

  render() {
    return (
      <main>
        <Switch>
          <Route exact path='/' component={Home}/>
          <Route path='/user' component={Userdetail}/>
          <Route path='/user:userId' component={Userdetail}/>
        </Switch>
      </main>
    );
  }
}