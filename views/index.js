import React from 'react';
import ReactDOM from 'react-dom';
import App from './master/App.js';
import { BrowserRouter } from 'react-router-dom'

require("../public/styles/main.less");

ReactDOM.render( (
	<BrowserRouter>
    <App />
  </BrowserRouter>
), document.getElementById('root'));